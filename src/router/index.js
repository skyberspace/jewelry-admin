import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Auth/Login.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Anasayfa',
    component: Home
  },

  {
    path: '/login',
    name: 'Giriş Yap',
    component: Login
  },
  {
    path: '/urunler',
    name: 'Ürün Listesi',
    component: () => import(/* webpackChunkName: "products" */ '../views/Product/Products.vue')
  },

  {
    path: '/kullanicilar',
    name: 'Kullanıcı Listesi',
    component: () => import(/* webpackChunkName: "users" */ '../views/User/Users.vue')
  },
  {
    path: '/kullanici/:id',
    name: 'Kullanıcı Detayı',
    props: true,
    component: () => import(/* webpackChunkName: "users" */ '../views/User/UserDetail.vue')
  },

  {
    path: '/siparisler',
    name: 'Sipariş Listesi',
    props: true,
    component: () => import(/* webpackChunkName: "orders" */ '../views/Order/Orders.vue')
  },

  {
    path: '/siparis/:id',
    name: 'Sipariş Detayı',
    props: true,
    component: () => import(/* webpackChunkName: "orders" */ '../views/Order/OrderDetail.vue')
  },
  {
    path: '/kategoriler',
    name: 'Kategori Listesi',
    component: () => import(/* webpackChunkName: "categories" */ '../views/Category/Categories.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
