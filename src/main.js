import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import '@/assets/scss/custom.scss';
import api from './api'
import Fragment from 'vue-fragment'
import vueCookie from "vue-cookie";

require("./vue-filter");


Vue.use(vueCookie);
Vue.use(Fragment.Plugin)
Vue.use(VueMaterial)


Vue.config.productionTip = false


Vue.prototype.$api = api;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
