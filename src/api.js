import axios from "axios";
import store from "./store";
import Vue from "vue";
const appRooturl = "https://localhost:44308/";
const appRooturlRel = "https://jevelleryapi.skyberspace.com/";

import Swal from 'sweetalert2'

export class Api {
    gen(id) {
        return this.generateApiUrl('Image/ProductCover/' + id)
    }
    generateApiUrl(operation) {
        var rootUrl = "";
        if (window.location.href.indexOf("//localhost") !== -1) {
            rootUrl = appRooturl;
        } else rootUrl = appRooturlRel;

        return rootUrl + operation;
    }

    async Request(url, payload = {}, method = "post") {
        method = method.toLowerCase();
        if (method === "post") {
            const res = await axios.post(this.generateApiUrl(url), payload);
            return res.data;
        }
        if (method === "get") {
            const res = await axios.get(this.generateApiUrl(url), payload);
            return res.data;
        }
    }
    async RequestWithOptions(url, payload = {}, options = {}) {
        return axios.post(this.generateApiUrl(url), payload, options);
    }
}
axios.interceptors.request.use(
    config => {
        //console.log(config);
        if (config.data)
            config.data.userToken = Vue.cookie.get("userToken");
        return config;
    },
    error => {
        // Do something with request error
        return Promise.reject(error);
    }
);

axios.interceptors.response.use(
    response => {
        if (response.data == null) {
            return response;
        }
        if (response.data.redirectToLogin) {
            store.commit("Logout");
            Promise.reject(response);
            Swal.fire("Hata", "Önce Giriş Yapmalısınız!", "error");
        }
        if (response.data.success === false && !response.data.redirectToLogin) {
            console.log("#################### HATA OLUŞTU ###################");
            console.log("#################### HATA MESAJI ###################");
            console.log(response);
            console.log("################# HATA MESAJI BİTTİ ################");
            if (response.data.message) {
                // @ts-ignore
                Swal.fire("Hata", response.data.message, "error");
            } else {
                // @ts-ignore
                Swal.fire("Hata", "Bir hata oluştu.", "error");
            }
            Promise.reject(response);
        }
        return response;
    },
    error => {
        console.log("#################### HATA OLUŞTU ###################");
        console.log("#################### HATA MESAJI ###################");
        console.log(error.response);
        console.log("################# HATA MESAJI BİTTİ ################");
        // @ts-ignore
        Swal.fire("Hata", "Bir hata oluştu.", "error");
        return Promise.reject(error);
    }
);

const api = new Api();
export default api;
